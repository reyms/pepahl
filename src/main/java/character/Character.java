package character;


import character.Inventory.Inventory.Inventory;

public class Character {
	// Inventar
	// Health
	// age
	// Statur
	// Religion
	// Beruf
	// Familienstand

	public enum Statue {
		small,
		slender,
		normal,
		bulky,
	}

	public enum Religion {
		Kerul,
		Hassim
	}

	private Inventory inventory;
	private int Health;
	private int age;
	private Statue statue;
	private Religion religion;

	public Character(Inventory inventory, int health, int age, Statue statue, Religion religion) {
		this.inventory = inventory;
		Health = health;
		this.age = age;
		this.statue = statue;
		this.religion = religion;
	}
}
